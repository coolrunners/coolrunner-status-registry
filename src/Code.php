<?php

namespace CoolRunner\Status;


class Code
    implements \JsonSerializable {
    protected $code;
    protected $message;
    protected $http_code;

    public function __construct(string $code, string $message, int $http_code) {
        $this->code = $code;
        $this->message = $message;
        $this->http_code = $http_code;
    }

    /**
     * Get the internal status code
     *
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * Get the status HTTP response code
     *
     * @return string
     */
    public function getHttpCode() {
        return $this->http_code;
    }

    /**
     * Get the status message
     *
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    public function toArray() {
        return [
            'status_code' => $this->code, 'status_message' => $this->message, 'response_code' => $this->http_code
        ];
    }

    public function jsonSerialize() {
        return $this->toArray();
    }

    protected static $descriptors = [
        'GE' => 'Generic',
        'CR' => 'CoolRunner',
        'SP' => 'Servicepoint',
        'SH' => 'Shipment',
        'CA' => 'Carrier',
        'TR' => 'Tracking'
    ];

    public static function getDescriptor(string $code) {
        return static::$descriptors[explode('_', $code)[0]] ?? null;
    }
}