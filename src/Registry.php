<?php

namespace CoolRunner\Status;


class Registry {

    /** @var CodeCollection|Code[] $codes */
    protected static $codes = [];

    protected static $default_code = 'CR_0001';

    public static function __autoload() {
        static::$codes = new CodeCollection();
        $files = glob(__DIR__ . '/../status/*.csv');

        static::loadGenericCodes();
        foreach ($files as $file) {
            static::loadCsv($file);
        }
    }

    public static function registerStatusCode($code, $message, $http_code) {
        static::$codes[$code] = new Code($code, $message, $http_code);

        return true;
    }

    public static function getStatus(string $code) {
        return static::$codes[$code] ?? static::$codes[static::$default_code];
    }

    public static function getAllStatuses() {
        return static::$codes;
    }

    protected static function loadCsv(string $file) {
        $handle = fopen($file, 'r');
        $lines = [];
        while (!feof($handle)) {
            $line = fgets($handle);
            if ($line && (count($csv_arr = array_map('trim', str_getcsv($line))) === 3)) {
                if (count($csv_arr)) {
                    $lines[] = $csv_arr;
                }
            }
        }
        fclose($handle);

        $lines = array_slice($lines, 1);

        foreach ($lines as $line) {
            if (array_filter($line)) {
                $code = mb_strtoupper($line[0]);
                $message = $line[1] ?: HttpMessages::getMessage($line[2]);
                $http = $line[2];
                static::registerStatusCode($code, $message, intval($http));
            }
        }

        return true;
    }

    protected static function loadGenericCodes() {
        $codes = [
            ['CR_0000', 'OK', 200],
            ['CR_0001', 'Internal Error', 500],
            ['CR_0002', 'Created', 201],
            ['CR_0003', 'Continue', 100],
            ['CR_0004', 'Bad Request', 400],
            ['CR_0005', 'Unauthorized', 401],
            ['CR_0006', 'Forbidden', 403],
            ['CR_0007', 'Not Found', 404],
            ['CR_0008', 'Too Many Requests', 429],
            ['CR_0009', 'Validation Failed', 422],
            ['CR_0420', 'Enhance the calm', 429],
            ['CR_1337', 'I\'m a little teapot', 418],
            ['CR_2300', 'Validation failed', 422]
        ];

        foreach ($codes as $code) {
            static::registerStatusCode($code[0], $code[1], $code[2]);
        }

        $key = 'GE';
        foreach (HttpMessages::getMessages() as $code => $message) {
            static::registerStatusCode("{$key}_" . str_pad($code, 4, 0, STR_PAD_LEFT), $message, $code);
        }
    }
}