### CA | Carrier
| Status | HTTP | Message |
| :-: | :-: | --- |
| CA_0000 | 200 | OK |
| CA_0001 | 404 | Unknown Carrier |
| CA_0002 | 400 | Feature Not Supported |
| CA_0003 | 500 | Unknown |
| CA_0004 | 401 | Invalid Credentials |
### CR | CoolRunner
| Status | HTTP | Message |
| :-: | :-: | --- |
| CR_0000 | 200 | OK |
| CR_0001 | 500 | Internal Error |
| CR_0002 | 201 | Created |
| CR_0003 | 100 | Continue |
| CR_0004 | 400 | Bad Request |
| CR_0005 | 401 | Unauthorized |
| CR_0006 | 403 | Forbidden |
| CR_0007 | 404 | Not Found |
| CR_0008 | 429 | Too Many Requests |
| CR_0009 | 422 | Validation Failed |
| CR_0420 | 429 | Enhance the calm |
| CR_1337 | 418 | I'm a little teapot |
| CR_2300 | 422 | Validation failed |
### GE | Generic
| Status | HTTP | Message |
| :-: | :-: | --- |
| GE_0100 | 100 | Continue |
| GE_0101 | 101 | Switching Protocols |
| GE_0102 | 102 | Processing |
| GE_0103 | 103 | Early Hints |
| GE_0200 | 200 | OK |
| GE_0201 | 201 | Created |
| GE_0202 | 202 | Accepted |
| GE_0203 | 203 | Non-Authoritative Information |
| GE_0204 | 204 | No Content |
| GE_0205 | 205 | Reset Content |
| GE_0206 | 206 | Partial Content |
| GE_0207 | 207 | Multi-Status |
| GE_0208 | 208 | Already Reported |
| GE_0226 | 226 | IM Used |
| GE_0300 | 300 | Multiple Choices |
| GE_0301 | 301 | Moved Permanently |
| GE_0302 | 302 | Found |
| GE_0303 | 303 | See Other |
| GE_0304 | 304 | Not Modified |
| GE_0305 | 305 | Use Proxy |
| GE_0307 | 307 | Temporary Redirect |
| GE_0308 | 308 | Permanent Redirect |
| GE_0400 | 400 | Bad Request |
| GE_0401 | 401 | Unauthorized |
| GE_0402 | 402 | Payment Required |
| GE_0403 | 403 | Forbidden |
| GE_0404 | 404 | Not Found |
| GE_0405 | 405 | Method Not Allowed |
| GE_0406 | 406 | Not Acceptable |
| GE_0407 | 407 | Proxy Authentication Required |
| GE_0408 | 408 | Request Timeout |
| GE_0409 | 409 | Conflict |
| GE_0410 | 410 | Gone |
| GE_0411 | 411 | Length Required |
| GE_0412 | 412 | Precondition Failed |
| GE_0413 | 413 | Payload Too Large |
| GE_0414 | 414 | URI Too Long |
| GE_0415 | 415 | Unsupported Media Type |
| GE_0416 | 416 | Range Not Satisfiable |
| GE_0417 | 417 | Expectation Failed |
| GE_0418 | 418 | I'm a teapot |
| GE_0421 | 421 | Misdirected Request |
| GE_0422 | 422 | Unprocessable Entity |
| GE_0423 | 423 | Locked |
| GE_0424 | 424 | Failed Dependency |
| GE_0425 | 425 | Reserved for WebDAV advanced collections expired proposal |
| GE_0426 | 426 | Upgrade Required |
| GE_0428 | 428 | Precondition Required |
| GE_0429 | 429 | Too Many Requests |
| GE_0431 | 431 | Request Header Fields Too Large |
| GE_0451 | 451 | Unavailable For Legal Reasons |
| GE_0500 | 500 | Internal Server Error |
| GE_0501 | 501 | Not Implemented |
| GE_0502 | 502 | Bad Gateway |
| GE_0503 | 503 | Service Unavailable |
| GE_0504 | 504 | Gateway Timeout |
| GE_0505 | 505 | HTTP Version Not Supported |
| GE_0506 | 506 | Variant Also Negotiates |
| GE_0507 | 507 | Insufficient Storage |
| GE_0508 | 508 | Loop Detected |
| GE_0510 | 510 | Not Extended |
| GE_0511 | 511 | Network Authentication Required |
### SH | Shipment
| Status | HTTP | Message |
| :-: | :-: | --- |
| SH_0000 | 200 | OK |
| SH_0001 | 500 | Unknown Error |
| SH_0002 | 404 | Package Number Not Found |
| SH_0003 | 404 | Tracking Unavailable |
| SH_0004 | 200 | Shipment Validated |
| SH_0005 | 201 | Shipment Created |
| SH_0006 | 400 | No Such Carrier |
| SH_0007 | 404 | No Viable Product Found |
### SP | Servicepoint
| Status | HTTP | Message |
| :-: | :-: | --- |
| SP_0000 | 200 | OK |
| SP_0001 | 500 | Unknown Error |
| SP_0002 | 400 | Invalid Servicepoint |
| SP_0003 | 404 | Not Found |
| SP_0004 | 422 | Invalid Zip Code |
| SP_0005 | 422 | Invalid Country Code |
### TR | Tracking
| Status | HTTP | Message |
| :-: | :-: | --- |
| TR_0000 | 200 | OK |
| TR_0001 | 500 | Unknown Error |
| TR_0002 | 404 | Tracking Unavailable |
| TR_0003 | 400 | Tracking Not Supported |
| TR_0004 | 404 | Package Number Not Found |
