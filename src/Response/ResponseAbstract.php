<?php

namespace CoolRunner\Status\Response;


use CoolRunner\Status\Registry;
use Symfony\Component\HttpFoundation\Response;

abstract class ResponseAbstract {
    protected $content;
    protected $code;
    protected $trace;

    /**
     * ResponseAbstract constructor.
     *
     * Create a new standardized response object
     *
     * @param mixed  $content
     * @param string $status_code
     */
    public function __construct($content, string $status_code = STATUS_CR_0000) {
        $this->code = Registry::getStatus($status_code);
        $this->content = $content;
    }

    /**
     * Static constructor
     *
     * See the non-static constructor
     *
     * @param mixed  $content
     * @param string $status_code
     *
     * @return static
     */
    public static function create($content, string $status_code = STATUS_CR_0000) {
        return (new static($content, $status_code));
    }

    /**
     * Static constructor
     *
     * See the non-static constructor
     *
     * @param mixed  $content
     * @param string $status_code
     *
     * @return Response
     */
    public static function push($content, string $status_code = STATUS_CR_0000) {
        return static::create($content,$status_code)->send();
    }

    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    public function setCode(string $code) {
        $this->code = $code;

        return $this;
    }

    public function setTrace($trace) {
        $this->trace = $trace;

        return $this;
    }

    public abstract function send()
    : Response;
}