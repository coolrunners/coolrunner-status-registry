<?php
/**
 * @package   coolrunner-error-registry
 * @author    Morten Harders
 * @copyright 2018
 */

namespace CoolRunner\Status\Response;


use Symfony\Component\HttpFoundation\JsonResponse as BaseJsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonResponse extends ResponseAbstract implements \JsonSerializable {

    public function jsonSerialize() {
        $return = $this->code->toArray();
        if ($this->content instanceof \JsonSerializable) {
            $return['response_data'] = $this->content->jsonSerialize();
        } elseif (method_exists($this->content, 'toArray')) {
            $return['response_data'] = $this->content->toArray();
        } elseif (method_exists($this->content, 'toJson') && is_string($json = $this->content->toJson())) {
            $return['response_data'] = json_decode($json, true);
        } else {
            $return['response_data'] = $this->content;
        }

        if (isset($this->trace)) {
            $return['trace'] = $this->trace;
        }

        return $return;
    }

    public function toJson($options = 0) {
        return json_encode($this->jsonSerialize(), $options);
    }

    public function send()
    : Response {
        return BaseJsonResponse::create($this, $this->code->getHttpCode(),
                                        [
                                            'Content-Type'       => 'application/json',
                                            'X-Response-Code'    => $this->code->getCode(),
                                            'X-Response-Message' => $this->code->getMessage()
                                        ]
        )->send();
    }

    public function __toString() {
        return $this->toJson();
    }

}