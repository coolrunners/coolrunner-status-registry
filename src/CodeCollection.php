<?php
/**
 * @package   coolrunner-error-registry
 * @author    Morten Harders
 * @copyright 2018
 */

namespace CoolRunner\Status;


class CodeCollection implements \ArrayAccess, \Countable, \Iterator, \JsonSerializable {
    protected $data = [];

    public function __construct(array $arr = []) {
        $this->data = $arr;
    }

    public function codeExists(string $code) {
        return $this->offsetExists($code);
    }

    public function groupExists(string $group) {
        foreach (array_keys($this->data) as $key) {
            if (mb_strpos(mb_strtolower($key), mb_strtolower($group)) !== false) {
                return true;
            }
        }

        return false;
    }

    // region JsonSerializable

    public function toArray() {
        return $this->data;
    }

    public function jsonSerialize() {
        return $this->toArray();
    }

    // endregion

    // region ArrayAccess

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->data[$offset] = $value;

        uksort($this->data, function ($a, $b) {
            return strcmp($a, $b);
        });
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    // endregion

    // region Countable

    public function count() {
        return count($this->data);
    }

    // endregion

    // region Iterator

    public function next() {
        next($this->data);
    }

    public function current() {
        return current($this->data);
    }

    public function key() {
        return key($this->data);
    }

    public function rewind() {
        reset($this->data);
    }

    public function valid() {
        return $this->offsetExists($this->key());
    }

    // endregion
}