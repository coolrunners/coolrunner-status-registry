<?php 
// +--------------------------+
// | - - -- - - CA - - -- - - |
// +--------------------------+
/** 200 | OK */
define('STATUS_CA_0000', 'CA_0000');

/** 404 | Unknown Carrier */
define('STATUS_CA_0001', 'CA_0001');

/** 400 | Feature Not Supported */
define('STATUS_CA_0002', 'CA_0002');

/** 500 | Unknown */
define('STATUS_CA_0003', 'CA_0003');

/** 401 | Invalid Credentials */
define('STATUS_CA_0004', 'CA_0004');

// +--------------------------+
// | - - -- - - CR - - -- - - |
// +--------------------------+
/** 200 | OK */
define('STATUS_CR_0000', 'CR_0000');

/** 500 | Internal Error */
define('STATUS_CR_0001', 'CR_0001');

/** 201 | Created */
define('STATUS_CR_0002', 'CR_0002');

/** 100 | Continue */
define('STATUS_CR_0003', 'CR_0003');

/** 400 | Bad Request */
define('STATUS_CR_0004', 'CR_0004');

/** 401 | Unauthorized */
define('STATUS_CR_0005', 'CR_0005');

/** 403 | Forbidden */
define('STATUS_CR_0006', 'CR_0006');

/** 404 | Not Found */
define('STATUS_CR_0007', 'CR_0007');

/** 429 | Too Many Requests */
define('STATUS_CR_0008', 'CR_0008');

/** 422 | Validation Failed */
define('STATUS_CR_0009', 'CR_0009');

/** 429 | Enhance the calm */
define('STATUS_CR_0420', 'CR_0420');

/** 418 | I'm a little teapot */
define('STATUS_CR_1337', 'CR_1337');

/** 422 | Validation failed */
define('STATUS_CR_2300', 'CR_2300');

// +--------------------------+
// | - - -- - - GE - - -- - - |
// +--------------------------+
/** 100 | Continue */
define('STATUS_GE_0100', 'GE_0100');

/** 101 | Switching Protocols */
define('STATUS_GE_0101', 'GE_0101');

/** 102 | Processing */
define('STATUS_GE_0102', 'GE_0102');

/** 103 | Early Hints */
define('STATUS_GE_0103', 'GE_0103');

/** 200 | OK */
define('STATUS_GE_0200', 'GE_0200');

/** 201 | Created */
define('STATUS_GE_0201', 'GE_0201');

/** 202 | Accepted */
define('STATUS_GE_0202', 'GE_0202');

/** 203 | Non-Authoritative Information */
define('STATUS_GE_0203', 'GE_0203');

/** 204 | No Content */
define('STATUS_GE_0204', 'GE_0204');

/** 205 | Reset Content */
define('STATUS_GE_0205', 'GE_0205');

/** 206 | Partial Content */
define('STATUS_GE_0206', 'GE_0206');

/** 207 | Multi-Status */
define('STATUS_GE_0207', 'GE_0207');

/** 208 | Already Reported */
define('STATUS_GE_0208', 'GE_0208');

/** 226 | IM Used */
define('STATUS_GE_0226', 'GE_0226');

/** 300 | Multiple Choices */
define('STATUS_GE_0300', 'GE_0300');

/** 301 | Moved Permanently */
define('STATUS_GE_0301', 'GE_0301');

/** 302 | Found */
define('STATUS_GE_0302', 'GE_0302');

/** 303 | See Other */
define('STATUS_GE_0303', 'GE_0303');

/** 304 | Not Modified */
define('STATUS_GE_0304', 'GE_0304');

/** 305 | Use Proxy */
define('STATUS_GE_0305', 'GE_0305');

/** 307 | Temporary Redirect */
define('STATUS_GE_0307', 'GE_0307');

/** 308 | Permanent Redirect */
define('STATUS_GE_0308', 'GE_0308');

/** 400 | Bad Request */
define('STATUS_GE_0400', 'GE_0400');

/** 401 | Unauthorized */
define('STATUS_GE_0401', 'GE_0401');

/** 402 | Payment Required */
define('STATUS_GE_0402', 'GE_0402');

/** 403 | Forbidden */
define('STATUS_GE_0403', 'GE_0403');

/** 404 | Not Found */
define('STATUS_GE_0404', 'GE_0404');

/** 405 | Method Not Allowed */
define('STATUS_GE_0405', 'GE_0405');

/** 406 | Not Acceptable */
define('STATUS_GE_0406', 'GE_0406');

/** 407 | Proxy Authentication Required */
define('STATUS_GE_0407', 'GE_0407');

/** 408 | Request Timeout */
define('STATUS_GE_0408', 'GE_0408');

/** 409 | Conflict */
define('STATUS_GE_0409', 'GE_0409');

/** 410 | Gone */
define('STATUS_GE_0410', 'GE_0410');

/** 411 | Length Required */
define('STATUS_GE_0411', 'GE_0411');

/** 412 | Precondition Failed */
define('STATUS_GE_0412', 'GE_0412');

/** 413 | Payload Too Large */
define('STATUS_GE_0413', 'GE_0413');

/** 414 | URI Too Long */
define('STATUS_GE_0414', 'GE_0414');

/** 415 | Unsupported Media Type */
define('STATUS_GE_0415', 'GE_0415');

/** 416 | Range Not Satisfiable */
define('STATUS_GE_0416', 'GE_0416');

/** 417 | Expectation Failed */
define('STATUS_GE_0417', 'GE_0417');

/** 418 | I'm a teapot */
define('STATUS_GE_0418', 'GE_0418');

/** 421 | Misdirected Request */
define('STATUS_GE_0421', 'GE_0421');

/** 422 | Unprocessable Entity */
define('STATUS_GE_0422', 'GE_0422');

/** 423 | Locked */
define('STATUS_GE_0423', 'GE_0423');

/** 424 | Failed Dependency */
define('STATUS_GE_0424', 'GE_0424');

/** 425 | Reserved for WebDAV advanced collections expired proposal */
define('STATUS_GE_0425', 'GE_0425');

/** 426 | Upgrade Required */
define('STATUS_GE_0426', 'GE_0426');

/** 428 | Precondition Required */
define('STATUS_GE_0428', 'GE_0428');

/** 429 | Too Many Requests */
define('STATUS_GE_0429', 'GE_0429');

/** 431 | Request Header Fields Too Large */
define('STATUS_GE_0431', 'GE_0431');

/** 451 | Unavailable For Legal Reasons */
define('STATUS_GE_0451', 'GE_0451');

/** 500 | Internal Server Error */
define('STATUS_GE_0500', 'GE_0500');

/** 501 | Not Implemented */
define('STATUS_GE_0501', 'GE_0501');

/** 502 | Bad Gateway */
define('STATUS_GE_0502', 'GE_0502');

/** 503 | Service Unavailable */
define('STATUS_GE_0503', 'GE_0503');

/** 504 | Gateway Timeout */
define('STATUS_GE_0504', 'GE_0504');

/** 505 | HTTP Version Not Supported */
define('STATUS_GE_0505', 'GE_0505');

/** 506 | Variant Also Negotiates */
define('STATUS_GE_0506', 'GE_0506');

/** 507 | Insufficient Storage */
define('STATUS_GE_0507', 'GE_0507');

/** 508 | Loop Detected */
define('STATUS_GE_0508', 'GE_0508');

/** 510 | Not Extended */
define('STATUS_GE_0510', 'GE_0510');

/** 511 | Network Authentication Required */
define('STATUS_GE_0511', 'GE_0511');

// +--------------------------+
// | - - -- - - SH - - -- - - |
// +--------------------------+
/** 200 | OK */
define('STATUS_SH_0000', 'SH_0000');

/** 500 | Unknown Error */
define('STATUS_SH_0001', 'SH_0001');

/** 404 | Package Number Not Found */
define('STATUS_SH_0002', 'SH_0002');

/** 404 | Tracking Unavailable */
define('STATUS_SH_0003', 'SH_0003');

/** 200 | Shipment Validated */
define('STATUS_SH_0004', 'SH_0004');

/** 201 | Shipment Created */
define('STATUS_SH_0005', 'SH_0005');

/** 400 | No Such Carrier */
define('STATUS_SH_0006', 'SH_0006');

/** 404 | No Viable Product Found */
define('STATUS_SH_0007', 'SH_0007');

// +--------------------------+
// | - - -- - - SP - - -- - - |
// +--------------------------+
/** 200 | OK */
define('STATUS_SP_0000', 'SP_0000');

/** 500 | Unknown Error */
define('STATUS_SP_0001', 'SP_0001');

/** 400 | Invalid Servicepoint */
define('STATUS_SP_0002', 'SP_0002');

/** 404 | Not Found */
define('STATUS_SP_0003', 'SP_0003');

/** 422 | Invalid Zip Code */
define('STATUS_SP_0004', 'SP_0004');

/** 422 | Invalid Country Code */
define('STATUS_SP_0005', 'SP_0005');

// +--------------------------+
// | - - -- - - TR - - -- - - |
// +--------------------------+
/** 200 | OK */
define('STATUS_TR_0000', 'TR_0000');

/** 500 | Unknown Error */
define('STATUS_TR_0001', 'TR_0001');

/** 404 | Tracking Unavailable */
define('STATUS_TR_0002', 'TR_0002');

/** 400 | Tracking Not Supported */
define('STATUS_TR_0003', 'TR_0003');

/** 404 | Package Number Not Found */
define('STATUS_TR_0004', 'TR_0004');

